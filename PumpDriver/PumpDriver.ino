/*****************************************************************************
 *
 *    This file is part of "PumpUNO", 
 *    a peristaltic pump driver for Arduino UNO.
 *
 *    PumpUNO is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    PumpUNO is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

// https://github.com/sparkfun/SparkFun_TB6612FNG_Arduino_Library
#include <SparkFun_TB6612.h>

const char* VERSION_STRING = "v0.3";

enum PIN {
 // TB6612 driver module I:
 //Channel A:
  AIN1_I = 7,
  AIN2_I = 8,
  PWMA_I = 9,
 //Channel B:
  BIN1_I = 5,
  BIN2_I = 4,
  PWMB_I = 3,
 // Mobule I Stand-By pin:
  STBY_I = 6,

 // TB6612 driver module II:
 //Channel A:
  AIN1_II = A3,
  AIN2_II = A4,
  PWMA_II = 10,
 //Channel B:
  BIN1_II = A1,
  BIN2_II = A0,
  PWMB_II = 11,
 // Mobule II Stand-By pin:
  STBY_II = A2,

  BUTTON_GREEN  = 12,
  BUTTON_ORANGE = 13,
  BUTTON_YELLOW = A5,
};

// Flip motor/pump direction, if necessary:
const int dirA_I  = 1;
const int dirB_I  = 1;
const int dirA_II = 1;
const int dirB_II = 1;

// Initialize objects:
Motor pump1 = Motor(AIN1_I,  AIN2_I,  PWMA_I,  dirA_I,  STBY_I);
Motor pump2 = Motor(BIN1_I,  BIN2_I,  PWMB_I,  dirB_I,  STBY_I);
Motor pump3 = Motor(AIN1_II, AIN2_II, PWMA_II, dirA_II, STBY_II);
Motor pump4 = Motor(BIN1_II, BIN2_II, PWMB_II, dirB_II, STBY_II);

// Remember speed setting for the four pumps.
int  pumpSpeeds[] = {0,0,0,0,0};


void setup()
{
  Serial.begin(9600);
  Serial.print(F("\n"));
  Serial.print(F("**************************\n"));
  Serial.print(F("** Arduino Pump Control **\n"));
  Serial.print(F("**************************\n"));
  Serial.print(F("*  Version:"));
  Serial.print(VERSION_STRING);
  Serial.print(F("          *\n"));
  Serial.print(F("*    Built:"));
  Serial.print(__DATE__);
  Serial.print(F("   *\n"));
  Serial.print(F("**************************\n"));
  Serial.print(F("\n"));
  Serial.print(F("Enter '?' for help."));
  Serial.print(F("\n\n"));

  pinMode(BUTTON_ORANGE, INPUT_PULLUP);
  pinMode(BUTTON_YELLOW, INPUT_PULLUP);
  pinMode(BUTTON_GREEN , INPUT_PULLUP);

  while (Serial.available() > 0) Serial.read(); // discard old data in Serial buffer

 } // end setup()


void loop()
{
  handleSerial();
  handleButtons();
}

void handleButtons() {
  if (digitalRead(BUTTON_YELLOW) == LOW) yellow();
  if (digitalRead(BUTTON_ORANGE) == LOW) orange();
  if (digitalRead(BUTTON_GREEN ) == LOW) green();
}

void yellow() {
  Serial.print("Orange: Run pump 1 for 1 second  at  50% speed...");
  pump1.drive(128);
  delay(1000);
  pump1.brake();
  Serial.print("done.\n");
}

void orange() {
  Serial.print("Yellow: Run pump 2 for 2 seconds at  75% speed...");
  pump2.drive(192);
  delay(2000);
  pump2.brake();
  Serial.print("done.\n");
}

void green() {
  Serial.print("Green:  Run pump 3 for 3 seconds at 100% speed...");
  pump3.drive(255);
  delay(3000);
  pump3.brake();
  Serial.print("done.\n");
}

void handleSerial() {
  uint8_t  pumpID    = 0;
  if (Serial.available() > 0) {
    delay(100); // Wait for other characters to arrive in Serial buffer.
    int pumpChar = Serial.read();
    switch (pumpChar) {
      case 0x41: // "A"
        pumpID = 1;
      break;

      case 0x42: // "B"
        pumpID = 2;
      break;

      case 0x43: // "C"
        pumpID = 3;
      break;

      case 0x44: // "D"
        pumpID = 4;
      break;

      case 0x3F: // "?"
        Serial.println(F("Syntax Examples:"));
        Serial.println(F("A1    : Set pump A (pump 1) speed setting to 12.5%."));
        Serial.println(F("C8    : Set pump C (pump 3) speed setting to 100%."));
        Serial.println(F("G3500 : Run pumps for 3.5 seconds."));
      break;
     
      case 0x47: // "G" (Go!)
        int timeChar1 = Serial.read() - 0x30;
        int timeChar2 = Serial.read() - 0x30;
        int timeChar3 = Serial.read() - 0x30;
        int timeChar4 = Serial.read() - 0x30;
        int pumpDuration = timeChar1*1000 + timeChar2*100 + timeChar3*10 + timeChar4*1;
        Serial.print("Running pumps for ");
        Serial.print(pumpDuration);
        Serial.print(F("ms\n"));
        pump(pumpDuration);
      break;

     //case 0x45: // "E"
     default:
        Serial.print(pumpChar);
        Serial.println(F("??"));
      break;
    }
   
    if (pumpID != 0) {
      uint8_t speedChar = Serial.read();
      if (speedChar == 0x2D) { // Encountered "-" sign:
        int pumpSpeed = Serial.read() - 0x30; // 0x30 is "0", 0x31 is "1", etc..
        if (pumpSpeed <=8) pumpSpeeds[pumpID] = -32*pumpSpeed+1; // Arrays start at index 1. Max PWM is 255, not 256.
        if (pumpSpeed ==0) pumpSpeeds[pumpID] = 0;
      } else {// No minus sign:
        int pumpSpeed = speedChar - 0x30; // 0x30 is "0", 0x31 is "1", etc..
        if (pumpSpeed <=8) pumpSpeeds[pumpID] = 32*pumpSpeed-1; // Arrays start at index 1. Max PWM is 255, not 256.
        else if (pumpSpeed ==0) pumpSpeeds[pumpID] = 0;
        else Serial.println(F("ERROR: Use only '0' -'8' (0%-100%) for speed setting."));
      }
      int newLineChar = Serial.read();
      switch (newLineChar) {
        case 0x0A: // LINE_FEED
        case 0x0D: // CARRIAGE_RETURN
          Serial.print(F("Pump "));
          Serial.print(pumpID);   
          Serial.print(F(" Speed: "));
          if (pumpSpeeds[pumpID] > 0)
            Serial.print((pumpSpeeds[pumpID]+1)/2.56);  // Output values 12.5%, 25%, etc..
          if (pumpSpeeds[pumpID] ==0)
            Serial.print(F(" 0.0 "));
          if (pumpSpeeds[pumpID] < 0)
            Serial.print((pumpSpeeds[pumpID]-1)/2.56);  // Output values 12.5%, 25%, etc..
          Serial.print(F(" %\n"));
          while (Serial.available() > 0) Serial.read(); // discard other data
        break;

        default:
          Serial.println(F("ERROR: Only one digit (0-8) for speed setting."));
          while (Serial.available() > 0) Serial.read(); // discard other data
        break;
      } // end switch
    } // end if (pumpID...
  } // end if (serial.available()
} // end handleSerial()

void pump(uint16_t pumpDuration) {
  pump1.drive(pumpSpeeds[1]);
  pump2.drive(pumpSpeeds[2]);
  pump3.drive(pumpSpeeds[3]);
  pump4.drive(pumpSpeeds[4]);
  delay(pumpDuration);
  pump1.brake();
  pump2.brake();
  pump3.brake();
  pump4.brake();
}


void test()
{
  pump1.drive(100,1000);
  pump1.drive(255,1000);
  pump1.drive(50,1000);
  pump1.brake();delay(1000);
  pump1.drive(-100,1000);
  pump1.drive(-255,1000);
  pump1.drive(-50,1000);
  pump1.brake();delay(1000);

  pump2.drive(100,1000);
  pump2.drive(255,1000);
  pump2.drive(50,1000);
  pump2.brake();delay(1000);
  pump2.drive(-100,1000);
  pump2.drive(-255,1000);
  pump2.drive(-50,1000);
  pump2.brake();delay(1000);

  pump3.drive(100,1000);
  pump3.drive(255,1000);
  pump3.drive(50,1000);
  pump3.brake();delay(1000);
  pump3.drive(-100,1000);
  pump3.drive(-255,1000);
  pump3.drive(-50,1000);
  pump3.brake();delay(1000);
  
  pump4.drive(100,1000);
  pump4.drive(255,1000);
  pump4.drive(50,1000);
  pump4.brake();delay(1000);
  pump4.drive(-100,1000);
  pump4.drive(-255,1000);
  pump4.drive(-50,1000);
  pump4.brake();delay(1000);
  
} // end loop()
